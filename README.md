
# Android Emulator Version Test

Various Android Emulator releases regularly prevent the emulator from
booting in Docker CI environments like GitLab-CI or Travis-CI.  This
is a simple test of booting emulators using different releases of the
emulator.

